from tomcat

maintainer shabber

run apt-get update -y
run apt-get upgrade -y

copy target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/hello-scalatra.war

expose 8080

CMD ["catalina.sh", "run"]